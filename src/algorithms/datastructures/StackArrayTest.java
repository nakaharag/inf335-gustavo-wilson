/*
 * TRABALHO 02 - INF335 - Ambientes para Concep��o de Software
 * 
 * Gustavo Nakahara - 49401504-4
 * Wilson Costa - 9998968-90
 * 
 */

package StackArrayPackageTest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import StackArrayPackage.StackArray;

public class StackArrayTest {
	
	private static StackArray stack;
	
	// SETUP
	@BeforeEach
	void Setup() {
		stack = new StackArray();
	}
	
	// CONSTRUCTOR
	@Test
	void testConstructorWithValidSize() {
		StackArray customStack = new StackArray(50);
		assertEquals(50, customStack.getMaxSize());
	}
	
	@Test
	void testConstructorWithInvalidSize() {
		StackArray customStack = new StackArray(-2);
		assertEquals(10, customStack.getMaxSize());
	}
	
	// INITIAL TESTS
	@Test
	void testInitialEmpty() {
		assertTrue(stack.isEmpty());
	}
	
	@Test
	void testSize() {
		assertEquals(0, stack.size());
	}
	
	@Test
	void testMaxSize() {
		assertEquals(10, stack.getMaxSize());
	}
	
	// PEEK TESTS
	@Test
	void testEmptyStackPeek() {
		assertEquals(-1, stack.peek());
	}
	
	@Test
	void testEmptyStackPeekAfterPushAndPop() {
		stack.push(12);
		stack.pop();
		
		assertEquals(-1, stack.peek());
	}
	
	@Test
	void testEmptyStackPeekAfterMultiplePushAndPop() {
		int i = 0;
		while(i < 15) {
			stack.push(++i);
		}
		while(i > 0) {
			stack.pop();
			i--;
		}
		
		assertEquals(-1, stack.peek());
	}
	
	// PUSH AND PEEK TESTS
	@Test
	void testSinglePushAndPeek() {
		int value = 12;
		stack.push(value);
		
		assertEquals(value, stack.peek());
	}
	
	@Test
	void testMultiplePushAndPeek() {
		int i = 0;
		int j = 0;
		while (i < 10) {
			j = 1 + i;
			stack.push(j);
			i++;
		}

		assertEquals(j, stack.peek());
	}

	@Test
	void testPushOverhead() {
		int i = 0;
		int j = 0;
		while (i < 15) {
			j = 1 + i;
			stack.push(j);
			i++;
		}
		
		assertEquals(j, stack.peek());
	}
	
	// POP TESTS
	@Test
	void testSinglePushPopAndPeek() {
		int i = 5;
		int j = 10;
		
		stack.push(i);
		stack.push(j);

		assertEquals(j, stack.pop());
		assertEquals(i, stack.peek());
	}
	
	@Test
	void testMultiplePopAndPeek() {
		int i = 0;
		int j = 0;
		while (i < 15) {
			j = 1 + i;
			stack.push(j);
			i++;
		}
		
		while (i > 0) {
			assertEquals(i, stack.pop());
			i--;
		}
	}
	
	@Test
	void testPopEmptyStack() {
		assertEquals(-1, stack.pop());
	}
	
	@Test
	void testPopEmptyStackAfterMultiplePushAndPop() {
		int i = 0;
		while(i < 15) {
			stack.push(i++);
		}
		while(i > 0) {
			stack.pop();
			i--;
		}
		
		assertEquals(-1, stack.pop());
		assertEquals(-1, stack.pop());
	}
	
	// ISEMPTY AND ISFULL TESTS
	void testIsEmptyIsFull() {
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		stack.push(12);
		assertFalse(stack.isEmpty());
		assertTrue(stack.isFull());
		stack.pop();
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		stack.push(12);
		assertFalse(stack.isEmpty());
		assertTrue(stack.isFull());
		stack.peek();
		assertFalse(stack.isEmpty());
		assertTrue(stack.isFull());
	}
	
	// SIZE TESTS
	@Test
	void testSizeAfterPush() {
		int i = 0;
		while(i < 5){
			stack.push(i++);
		}
		
		assertEquals(5, stack.size());
		assertEquals(10, stack.getMaxSize());
	}
	
	@Test
	void testSizeAfterPushAndResize() {
		int i = 0;
		while(i < 15){
			stack.push(i++);
		}
		
		assertEquals(15, stack.size());
		assertEquals(20, stack.getMaxSize());
	}
	
	// MAKEEMPTY TESTS
	@Test
	void testMakeEmpty1() {
		stack.push(12);
		stack.makeEmpty();
		assertTrue(stack.isEmpty());
	}
	
	@Test
	void testMakeEmpty2() {
		stack.push(12);
		stack.makeEmpty();
		stack.push(13);
		assertEquals(13, stack.pop());
		assertEquals(-1, stack.pop());
	}
}
